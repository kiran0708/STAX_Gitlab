package CodeImplementation;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Base.BaseUtil;
import ObjectRepository.BookObjects;


public class Booking extends BaseUtil implements BookObjects {

	public Booking(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//a[@class='btn-sta btn-primary btn-book']")
	public WebElement BookNow;
	
	@FindBy(how = How.XPATH, using = "//input[@value='Book Now!']")
	public WebElement BookNowAtCheckout;

	@FindBy(how = How.XPATH, using = "//span[@class='tot-price ng-binding']")
	public WebElement PriceInBookingPage;

	@FindBy(how = How.XPATH, using = "//a[contains(.,'Continue')]")
	public WebElement Continue;
	
	@FindBy(how = How.XPATH, using = "//a[@class='btn btn-common disabled']")
	public List<WebElement> ContinueDisabled;

	@FindBy(how = How.XPATH, using = "//p[contains(.,'Pay deposit')]")
	public WebElement PayDeposit;

	@FindBy(how = How.XPATH, using = "//p[contains(.,'Pay full amount')]")
	public WebElement PayFullAmount;
	
	@FindBy(how = How.XPATH, using = "//input[@type='radio']")
	public WebElement HoldRB;
	
	@FindBy(how = How.XPATH, using = "//input[@value='Hold Request']")
	public WebElement HoldRequestButton;

	@FindBy(how = How.XPATH, using = "//span[contains(.,'PROCESSING BOOKING')]")
	public List<WebElement> ProcessingBooking;
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'PROCESSING DEPOSIT')]")
	public List<WebElement> ProcessingDeposit;
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'HOLD REQUEST')]")
	public List<WebElement> HoldBookingComplete;
	
	@FindBy(how = How.ID, using = "spanInvalidCardDetails")
	public List<WebElement> PaymentErrorPopUp;
	
	// The Passenger Details
	
	@FindBy(how = How.ID, using = "txtAFirstName_0")
	public WebElement FirstName;

	@FindBy(how = How.ID, using = "txtALastName_0")
	public WebElement LastName;

	@FindBy(how = How.ID, using = "ddlDOBADate_0")
	public WebElement Date_DOB;

	@FindBy(how = How.ID, using = "ddlDOBAMonth_0")
	public WebElement Month_DOB;

	@FindBy(how = How.ID, using = "ddlDOBAYear_0")
	public WebElement Year_DOB;

	@FindBy(how = How.ID, using = "txtPhoneNo")
	public WebElement PhoneNumber;

	@FindBy(how = How.ID, using = "txtEmailAddress")
	public WebElement EmailAddress;

	// The Credit Card Details
	
	@FindBy(how = How.ID, using = "txtCardName")
	public WebElement CardHolderName;

	@FindBy(how = How.ID, using = "txtCardNumber")
	public WebElement CardNumber;

	@FindBy(how = How.ID, using = "txtSecurityCode")
	public WebElement Cvv;
	
	@FindBy(how = How.XPATH, using = "//*[@id='ddlExpirationMonth']/option[9]")
	public WebElement CardExpMonth;
	
	@FindBy(how = How.XPATH, using = "//*[@id='ddlExpirationYear']/option[4]")
	public WebElement CardExpYear;
	
	// The Billing Details in Booking Page
	
	@FindBy(how = How.ID, using = "txtBillingStreet")
	public WebElement BillingStreet;
	
	@FindBy(how = How.ID, using = "txtBillingCity")
	public WebElement BillingCity;
	
	@FindBy(how = How.ID, using = "txtState")
	public List<WebElement> BillingState;
	
	@FindBy(how = How.ID, using = "txtZipCode")
	public WebElement ZipCode;
	
	
	@FindBy(how = How.ID, using = "chkCancellationPolicy")
	public WebElement CancellationPolicy;
	
	@FindBy(how = How.ID, using = "chkTermsofService")
	public WebElement TermsOfService;

	@FindBy(how = How.XPATH, using = "//p[@class='dep-less ng-scope']")
	public WebElement LessThanReqDaysForDeposit;
	
	@FindBy(how = How.XPATH, using = "//div[@class='day selectedDate selectable']")
	public List<WebElement> DepartureDatesInAMonth;
	
	@FindBy(how = How.XPATH, using = "//div/div[@class='day selectable']")
	public List<WebElement> DepartureDatesInAMonth1;
	
	@FindBy(how = How.XPATH, using = "//div[@class='day selectedDate selectable call-to-book']")
	public List<WebElement> CallToBookDepInAMonth;
	
	@FindBy(how = How.XPATH, using = "//div[@class='day selectable call-to-book']")
	public List<WebElement> CallToBookDepInAMonth1;
	
	@FindBy(how = How.XPATH, using = "//span[contains(.,'NEXT MONTH >')]")
	public WebElement NextMonth;
			
	@Override
	public String ClickBookNow() {
		WebDriverWait wait = new WebDriverWait(Driver, 20);
		//wait.until(ExpectedConditions.visibilityOf(BookNow));
		//wait.until(ExpectedConditions.elementToBeClickable(BookNow));
		try {
		Thread.sleep(8000);
		}
		catch(Exception e) {
			
		}
		BookNow.click();
		return null;
	}

	@Override
	public void ClickContinue() {
		Continue.click();
	}

	@Override
	public void selectPayDepositAmount() {
		Actions actions = new Actions(Driver);
		actions.moveToElement(PayDeposit).click().perform();
	}

	@Override
	public void selectPayFullAmount() {
		Actions actions = new Actions(Driver);
		actions.moveToElement(PayFullAmount).click().perform();
	}

	@Override
	public void EnterPassengerDetails() {
		FirstName.sendKeys("Test");
		LastName.sendKeys("Automation");
		Date_DOB.sendKeys("10");
		Month_DOB.sendKeys("6");
		Year_DOB.sendKeys("1992");
		PhoneNumber.sendKeys("01234567899");
		EmailAddress.sendKeys("statraveltestautomation@gmail.com");

	}

	@Override
	public void EnterValidCardDetailsStaging() {
		CardHolderName.sendKeys("Test QA");
		CardNumber.sendKeys("4111111111111111");
		Cvv.sendKeys("411");
		CardExpYear.click();
		CardExpMonth.click();
	}
	
	@Override
	public void EnterInvalidCardDetails() {
		CardHolderName.sendKeys("REFUSED");
		CardNumber.sendKeys("4111111111111111");
		Cvv.sendKeys("411");
		CardExpYear.click();
		CardExpMonth.click();
	}

	@Override
	public void EnterBillingDetails() {
		BillingStreet.sendKeys("TestStreet");
		BillingCity.sendKeys("TestCity");
		try {
			BillingState.get(0).sendKeys("TestState");
		}
		catch(Exception e) {
			
		}
		ZipCode.sendKeys("TZ00017");

	}

	@Override
	public void AcceptBookingTerms() {
		CancellationPolicy.click();
		TermsOfService.click();

	}

	@Override
	public void ClickBookAtCheckOut() {
		BookNowAtCheckout.click();
	}

	@Override
	public void ClickHoldRequest() {
		HoldRequestButton.click();
	}

	@Override
	public String GetPriceFromBookingPage() {
		String Price = PriceInBookingPage.getText();
		return Price;
	}

	@Override
	public void ClickNeedMoreTimeRB() {
		Actions actions = new Actions(Driver);
		actions.moveToElement(HoldRB).click().perform();		
	}

	@Override
	public void AdjustDepartureDateForHold() {
		while(HoldRB.isDisplayed()==false) {
			NextMonth.click();
			int departures1 = DepartureDatesInAMonth.size();
			int departures2 = DepartureDatesInAMonth1.size();
			System.out.println("Total Departures1 are : " + departures1);
			System.out.println("Total Departures2 are : " + departures2);
			DepartureDatesInAMonth1.get(departures2-1).click();
		}
	
		
	}

	public void getDepInMonth() {
		NextMonth.click();
		int departures1 = DepartureDatesInAMonth.size();
		int departures2 = DepartureDatesInAMonth1.size();
		System.out.println("Total Departures1 are : " + departures1);
		System.out.println("Total Departures2 are : " + departures2);
		DepartureDatesInAMonth1.get(departures2-1).click();
		
	}
	
	public void ContactUstobookinmonth(){
		int departures = CallToBookDepInAMonth.size() + CallToBookDepInAMonth1.size();
		System.out.println("Total contact us to book are : " + departures);
	}

	@Override
	public void AdjustDepartureDateForDeposit() {
		while(PayDeposit.isDisplayed()==false) {
			NextMonth.click();
			int departures1 = DepartureDatesInAMonth.size();
			int departures2 = DepartureDatesInAMonth1.size();
			System.out.println("Total Departures1 are : " + departures1);
			System.out.println("Total Departures2 are : " + departures2);
			DepartureDatesInAMonth1.get(departures2-1).click();
		}
		
	}

	@Override
	public void AdjustDepartureDateForFullPayment() {
				
		while(ContinueDisabled.size()>0) {
			NextMonth.click();
			int departures1 = DepartureDatesInAMonth.size();
			int departures2 = DepartureDatesInAMonth1.size();
			System.out.println("Total Departures1 are : " + departures1);
			System.out.println("Total Departures2 are : " + departures2);
			if(departures2>0) {
			DepartureDatesInAMonth1.get(departures2-1).click();
			}
		}
		
	}

	@Override
	public void VerifyFullPaymentBooking() {
		Assert.assertTrue(ProcessingBooking.size()>0);
		
	}

	@Override
	public void VerifyHoldBooking() {
		Assert.assertTrue(HoldBookingComplete.size()>0);
		
	}

	@Override
	public void VerifyDepositBooking() {
		Assert.assertTrue(ProcessingDeposit.size()>0);		
	}

	@Override
	public void VerifyInvalidPayment() {
		//System.out.println(PaymentErrorPopUp.get(0).getText());
		Assert.assertTrue(PaymentErrorPopUp.size()>0);
		
	}
	
}

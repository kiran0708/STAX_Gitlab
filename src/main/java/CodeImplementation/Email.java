package CodeImplementation;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import Base.BaseUtil;
import ObjectRepository.EmailObjects;

public class Email extends BaseUtil implements EmailObjects{

	public Email(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using ="//a[@class='emailPopUp btn-sta btn-email']") 
	public WebElement EmailUsButton;
	
	@FindBy(how = How.ID, using ="txteffirstname") 
	public  WebElement FirstName; 
	
	@FindBy(how = How.ID, using ="txteflastname") 
	public  WebElement LastName; 
	
	@FindBy(how = How.ID, using ="txtefemail") 
	public  WebElement EmailId; 
	
	@FindBy(how = How.ID, using ="txtefphone") 
	public  WebElement PhoneNo; 
	
	@FindBy(how = How.ID, using ="txtefinfo") 
	public  WebElement MoreInfo; 
		
	@FindBy(how = How.ID, using ="btnSubmit") 
	public  WebElement SendMyEmail; 
	
	@FindBy(how = How.XPATH, using ="//div/a[contains(.,'Close')]") 
	public  WebElement Close; 
	
	
			
	@Override
	public void ClickEmailUs() throws Exception {
		WebDriverWait wait = new WebDriverWait(Driver, 20);
		//wait.until(ExpectedConditions.visibilityOf(EmailUsButton));
		//wait.until(ExpectedConditions.elementToBeClickable(EmailUsButton));
		Thread.sleep(8000);
		EmailUsButton.click();		
	}

	@Override
	public void EnterEmailDetails() throws Exception {
		FirstName.sendKeys("Test");
		LastName.sendKeys("Automation");
		EmailId.sendKeys("statraveltestautomation@gmail.com");
		PhoneNo.sendKeys("01234567899");
		MoreInfo.sendKeys("Test Purpose, Please Ignore");		
	}

	@Override
	public void ClickSendMyEmail() throws Exception {
		SendMyEmail.click();		
	}

	@Override
	public void VerifyEmailSentText() {
		Driver.getPageSource().contains("Thank you for your enquiry");
		Driver.getPageSource().contains("jhajhdjhdjdhsjdh");		
	}

	@Override
	public void CloseEmailPopup() throws Exception {
		Close.click();	
	}
	
	public static boolean verifyMail(String userName, String password, String message) {
		Folder folder = null;
		Store store = null;
		System.out.println("***READING MAILBOX...");
		try {
			Properties props = new Properties();
			props.put("mail.store.protocol", "imaps");
			Session session = Session.getInstance(props);
			store = session.getStore("imaps");
			store.connect("imap.gmail.com", userName, password);
			folder = store.getFolder("INBOX");
			folder.open(Folder.READ_ONLY);
			Message[] messages = folder.getMessages();
			System.out.println("No of Messages : " + folder.getMessageCount());
			System.out.println("No of Unread Messages : " + folder.getUnreadMessageCount());
			for (int i = 0; i < messages.length; i++) {
				System.out.println("Reading MESSAGE # " + (i + 1) + "...");
				Message msg = messages[i];
				String strMailSubject = "", strMailBody = "";
				// Getting mail subject
				Object subject = msg.getSubject();
				// Getting mail body
				Object content = msg.getContent();
				// Casting objects of mail subject and body into String
				strMailSubject = (String) subject;
				//---- This is what you want to do------
				if (strMailSubject.contains(message)) {
					System.out.println(strMailSubject);
					break;
				}
			}
			return true;
		} catch (MessagingException messagingException) {
			messagingException.printStackTrace();
		} catch (IOException ioException) {
			ioException.printStackTrace();
		} finally {
			if (folder != null) {
				try {
					folder.close(true);
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (store != null) {
				try {
					store.close();
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return false;
	}

}

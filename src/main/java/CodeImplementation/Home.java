package CodeImplementation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import Base.BaseUtil;
import ObjectRepository.HomeObjects;

public class Home extends BaseUtil implements HomeObjects {

	public Home(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID, using = "txtSearch")
	public WebElement AutoSearch;

	@FindBy(how = How.XPATH, using = "//span[contains(@class,'trip-price-inner ng-binding')]")
	public List<WebElement> priceXpath;

	@FindBy(how = How.XPATH, using = "//span[@class='trip-days ng-binding']")
	public List<WebElement> daysXpath;

	@FindBy(how = How.XPATH, using = "//*[@id=\"result-count\"]/span")
	public WebElement TourCountInLabel;

	@FindBy(how = How.ID, using = "sortSelect")
	public WebElement SortDropDown;

	@FindBy(how = How.XPATH, using = "//*[@id=\"price-range\"]/div[1]/span[6]")
	public WebElement MinPriceSlider;

	@FindBy(how = How.XPATH, using = "//*[@id=\"price-range\"]/div[1]/span[7]")
	public WebElement MaxPriceSlider;

	@FindBy(how = How.XPATH, using = "//*[@id=\"price-range\"]/div[2]/div[2]/input")
	public WebElement MinPriceValue;

	@FindBy(how = How.XPATH, using = "//*[@id=\"price-range\"]/div[2]/div[6]/input")
	public WebElement MaxPriceValue;

	@FindBy(how = How.XPATH, using = "//*[@id=\"duration-range\"]/div[1]/span[6]")
	public WebElement MinDurationSlider;

	@FindBy(how = How.XPATH, using = "//*[@id=\"duration-range\"]/div[1]/span[7]")
	public WebElement MaxDurationSlider;

	@FindBy(how = How.XPATH, using = "//*[@id=\"duration-range\"]/div[2]/div[1]/input")
	public WebElement MinDurationValue;

	@FindBy(how = How.XPATH, using = "//*[@id=\"duration-range\"]/div[2]/div[4]/input")
	public WebElement MaxDurationValue;

	@FindBy(how = How.ID, using = "list")
	public WebElement ListViewIcon;

	@FindBy(how = How.ID, using = "grid")
	public WebElement GridViewIcon;

	@FindBy(how = How.XPATH, using = "//div[@class='result-item ng-scope offer-tour list-view-mode']")
	public List<WebElement> ListView;

	@FindBy(how = How.XPATH, using = "//div[@class='result-item ng-scope list-view-mode']")
	public List<WebElement> ListView1;

	@FindBy(how = How.XPATH, using = "//div[@class='result-item ng-scope offer-tour']")
	public List<WebElement> GridView;

	@FindBy(how = How.XPATH, using = "//div[@class='result-item ng-scope']")
	public List<WebElement> GridView1;
	
	@FindBy(how = How.XPATH, using = "//*[@id='frmDepDate']/div[4]/label/span[2]")
	public WebElement FourthMonthFromNow;

	public String SearchResult = "//li[contains(text(),'";

	@Override
	public void userSearchesonAutoSearch(String SearchString) {
		AutoSearch.sendKeys(SearchString);

	}

	@Override
	public void userSearchesCountryFromAutoSearch(String Country) {
		final By ResultXpath = By.xpath(SearchResult + Country + "')]");
		Driver.findElement(ResultXpath).click();

	}

	@Override
	public String SelectSortOption(String SortOption) throws InterruptedException {
		SortDropDown.click();
		Thread.sleep(4000);
		Select dropdown = new Select(SortDropDown);
		dropdown.selectByValue(SortOption);
		Thread.sleep(7000);
		return SortOption;
	}

	@Override
	public void VerifySortResults(String SortOption) {
		ArrayList<Integer> obtainedList = new ArrayList<>();
		List<WebElement> elementList = priceXpath;

		if (SortOption.equalsIgnoreCase("PriceLowest") || SortOption.equalsIgnoreCase("PriceHigest")) {
			for (WebElement we : elementList) {
				String value = we.getText();
				int Price = Integer.parseInt(value.replaceAll("[^0-9]", ""));
				obtainedList.add(Price);

			}
		}
		ArrayList<Integer> sortedList = new ArrayList<>();
		for (Integer s : obtainedList) {
			sortedList.add(s);
		}

		if (SortOption.equalsIgnoreCase("PriceLowest")) {
			Collections.sort(sortedList);
			Assert.assertEquals(obtainedList, sortedList);
		}

		if (SortOption.equalsIgnoreCase("PriceHigest")) {
			Collections.sort(sortedList, Collections.reverseOrder());
			Assert.assertEquals(obtainedList, sortedList);
		}

	}

	@Override
	public void VerifySortResultsPerDay(String SortOption) {

		ArrayList<Integer> PriceObtainedList = new ArrayList<>();
		ArrayList<Integer> DaysObtainedList = new ArrayList<>();

		List<WebElement> PriceList = priceXpath;
		List<WebElement> DaysList = daysXpath;

		for (WebElement we1 : PriceList) {
			String value = we1.getText();
			int Price = Integer.parseInt(value.replaceAll("[^0-9]", ""));
			PriceObtainedList.add(Price);

		}

		for (WebElement we2 : DaysList) {
			String value = we2.getText();
			int Days = Integer.parseInt(value.replaceAll("[^0-9]", ""));
			DaysObtainedList.add(Days);

		}

		ArrayList<Integer> ObtainedPricePerDay = new ArrayList<>();

		for (int x = 0; x < PriceObtainedList.size(); x++) {

			for (int j = 0; j < DaysObtainedList.size(); j++) {
				if (x == j) {
					int result = PriceObtainedList.get(x) / DaysObtainedList.get(j);
					ObtainedPricePerDay.add(result);
				}
			}
		}

		ArrayList<Integer> sortList = new ArrayList<>();
		for (Integer s : ObtainedPricePerDay) {
			sortList.add(s);
		}

		ArrayList<Integer> sortDaysList = new ArrayList<>();
		for (Integer s : DaysObtainedList) {
			sortDaysList.add(s);
		}

		if (SortOption.equalsIgnoreCase("PricePerDayLowest")) {
			Collections.sort(sortList);
			Assert.assertEquals(ObtainedPricePerDay, sortList);
		}

		if (SortOption.equalsIgnoreCase("PricePerDayHigest")) {
			Collections.sort(sortList, Collections.reverseOrder());
			Assert.assertEquals(ObtainedPricePerDay, sortList);

		}

		if (SortOption.equalsIgnoreCase("DurationLowest")) {
			Collections.sort(sortDaysList);
			Assert.assertEquals(DaysObtainedList, sortDaysList);

		}

	}

	@Override
	public void ClickReset() {
		// TODO Auto-generated method stub

	}

	@Override
	public void UserScrollTillTheResetButtonIsFound() throws InterruptedException {
		// TODO Auto-generated method stub

	}

	@Override
	public void VerifyCountOfTours() {
		// TODO Auto-generated method stub

	}

	@Override
	public void MovePriceSliderToLeft() throws Exception {
		Actions move = new Actions(Driver);
		Action action = (Action) move.dragAndDropBy(MaxPriceSlider, -40, 0).build();
		action.perform();
		Thread.sleep(3000);

	}

	@Override
	public void MovePriceSliderToRight() throws Exception {
		Actions move = new Actions(Driver);
		Action action = (Action) move.dragAndDropBy(MinPriceSlider, 20, 0).build();
		action.perform();
		Thread.sleep(3000);
	}

	@Override
	public void EnterLowestPriceRance() throws Exception {
		MinPriceValue.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE), "500");
		Thread.sleep(2000);

	}

	@Override
	public void EnterHighestPriceRance() throws Exception {
		MaxPriceValue.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE), "4500");
		Thread.sleep(2000);
	}

	@Override
	public void VerifyLowestPriceAccordingToPriceFilters() throws Exception {
		int MinRange = Integer.parseInt(MinPriceValue.getAttribute("value"));
		SelectSortOption("PriceLowest");
		Thread.sleep(4000);
		int LowPrice = Integer.parseInt(priceXpath.get(0).getText().replaceAll("[^0-9]", ""));
		Assert.assertTrue(LowPrice >= MinRange);
	}

	@Override
	public void VerifyHighestPriceAccordingToPriceFilters() throws Exception {
		int MaxRange = Integer.parseInt(MaxPriceValue.getAttribute("value"));
		SelectSortOption("PriceHigest");
		int HighPrice = Integer.parseInt(priceXpath.get(0).getText().replaceAll("[^0-9]", ""));
		Assert.assertTrue(HighPrice <= MaxRange);

	}

	@Override
	public void MoveDurationSliderToLeft() throws Exception {
		Actions move = new Actions(Driver);
		Action action = (Action) move.dragAndDropBy(MaxDurationSlider, -40, 0).build();
		action.perform();
		Thread.sleep(3000);
		Driver.findElement(By.xpath("//*[@id=\"duration-range\"]/div[2]/div[5]")).click();

	}

	@Override
	public void MoveDurationSliderToRight() throws Exception {
		Actions move = new Actions(Driver);
		Action action = (Action) move.dragAndDropBy(MinDurationSlider, 50, 0).build();
		action.perform();
		Thread.sleep(3000);
		Driver.findElement(By.xpath("//*[@id=\"duration-range\"]/div[2]/div[5]")).click();
	}

	@Override
	public void EnterLowestDuration() throws Exception {
		MinDurationValue.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE), "5");
		Thread.sleep(2000);

	}

	@Override
	public void EnterHighestDuration() throws Exception {
		MaxDurationValue.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE), "15");
		Thread.sleep(2000);

	}

	@Override
	public void VerifyLowestDurationAccordingToDurationFilters() throws Exception {
		int MinRange = Integer.parseInt(MinDurationValue.getAttribute("value"));
		SelectSortOption("DurationLowest");
		Thread.sleep(4000);
		int LowDuration = Integer.parseInt(daysXpath.get(0).getText().replaceAll("[^0-9]", ""));
		System.out.println("Min Duration is : " + LowDuration);
		Assert.assertTrue(LowDuration >= MinRange);

	}

	@Override
	public void VerifyHighestPriceAccordingToDurationFilters() throws Exception {
		int MaxRange = Integer.parseInt(MaxDurationValue.getAttribute("value"));

		String ResultsLabelDisplayed = TourCountInLabel.getText();

		int TotalResults = Driver.findElements(By.xpath("//div[@id='results-flex-grid']/div")).size();
		int valueInLabel = Integer.parseInt(ResultsLabelDisplayed);
		System.out.println("The value in label is: " + valueInLabel);

		while (TotalResults != valueInLabel) {
			int TotalResults1 = TotalResults;
			Thread.sleep(1000);

			Actions action = new Actions(Driver);
			action.sendKeys(Keys.PAGE_DOWN).build().perform();
			action.sendKeys(Keys.PAGE_DOWN).build().perform();
			TotalResults = Driver.findElements(By.xpath("//div[@id='results-flex-grid']/div")).size();
			if (TotalResults1 == TotalResults) {
				action.sendKeys(Keys.PAGE_UP).build().perform();
			}

		}
		int HighDuration = Integer.parseInt(daysXpath.get(valueInLabel - 1).getText().replaceAll("[^0-9]", ""));
		Assert.assertTrue(HighDuration <= MaxRange);

	}

	@Override
	public void ClickOnListView() throws Exception {
		ListViewIcon.click();
	}

	@Override
	public void ClickOnGridView() throws Exception {
		GridViewIcon.click();
	}

	@Override
	public void VerifyToursDisplayInListView() throws Exception {
		int a = ListView.size();
		int b = ListView1.size();
		int c = a + b;
		System.out.println("The count of tours in List is : " + c);
		Assert.assertTrue(c > 0);

	}

	@Override
	public void VerifyToursDisplayInGridView() throws Exception {
		int a = GridView.size();
		int b = GridView1.size();
		int c = a + b;
		System.out.println("The count of tours in Grid is : " + c);
		Assert.assertTrue(c > 0);
	}

	@Override
	public void SelectFourthMonthFromNow() {
		FourthMonthFromNow.click();
		
	}

}

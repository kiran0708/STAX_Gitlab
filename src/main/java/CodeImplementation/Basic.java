package CodeImplementation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import Base.BaseUtil;

public class Basic extends BaseUtil {

	public void SwitchToNewWindow() throws Exception {
		try {
			Set<String> handles = Driver.getWindowHandles();
			Iterator itr = handles.iterator();
			String parent_window = (String) itr.next();
			System.out.println(parent_window + "....." + Driver.getTitle());
			String child_window = (String) itr.next();
			Driver.switchTo().activeElement();
			Driver.switchTo().window(child_window);
		} catch (AssertionError ex) {
			
		}

	}

	public void LaunchUKURL(String Supplier) throws Exception {
		InputStream input = new FileInputStream("Utilities.properties");
		Properties prop = new Properties();
		prop.load(input);

		if (Supplier.equalsIgnoreCase("Contiki")) {
			Driver.navigate().to(prop.getProperty("Contiki_uk"));
		}

		if (Supplier.equalsIgnoreCase("GAdventures")) {
			Driver.navigate().to(prop.getProperty("GAdventures_uk"));
		}

		if (Supplier.equalsIgnoreCase("Dragoman")) {
			Driver.navigate().to(prop.getProperty("Dragoman_uk"));
		}

		if (Supplier.equalsIgnoreCase("1Source")) {
			Driver.navigate().to(prop.getProperty("1Source_uk"));
		}
		if (Supplier.equalsIgnoreCase("TrekAmerica")) {
			Driver.navigate().to(prop.getProperty("TrekAmerica_uk"));
		}
		if (Supplier.equalsIgnoreCase("GrandAmerica")) {
			Driver.navigate().to(prop.getProperty("GrandAmerica_uk"));
		}
		if (Supplier.equalsIgnoreCase("Radical")) {
			Driver.navigate().to(prop.getProperty("Radical_uk"));
		}

	}

	public void LaunchAUURL(String Supplier) throws Exception {
		InputStream input = new FileInputStream("Utilities.properties");
		Properties prop = new Properties();
		prop.load(input);
		if (Supplier.equalsIgnoreCase("Contiki")) {
			Driver.navigate().to(prop.getProperty("Contiki_au"));
		}

		if (Supplier.equalsIgnoreCase("GAdventures")) {
			Driver.navigate().to(prop.getProperty("GAdventures_au"));
		}

		if (Supplier.equalsIgnoreCase("Dragoman")) {
			Driver.navigate().to(prop.getProperty("Dragoman_au"));
		}

		if (Supplier.equalsIgnoreCase("1Source")) {
			Driver.navigate().to(prop.getProperty("1Source_au"));
		}
		if (Supplier.equalsIgnoreCase("TrekAmerica")) {
			Driver.navigate().to(prop.getProperty("TrekAmerica_au"));
		}
		if (Supplier.equalsIgnoreCase("GrandAmerica")) {
			Driver.navigate().to(prop.getProperty("GrandAmerica_au"));
		}
		if (Supplier.equalsIgnoreCase("Radical")) {
			Driver.navigate().to(prop.getProperty("Radical_au"));
		}

	}

	public void LaunchNZURL(String Supplier) throws Exception {
		InputStream input = new FileInputStream("Utilities.properties");
		Properties prop = new Properties();
		prop.load(input);
		if (Supplier.equalsIgnoreCase("Contiki")) {
			Driver.navigate().to(prop.getProperty("Contiki_nz"));
		}

		if (Supplier.equalsIgnoreCase("GAdventures")) {
			Driver.navigate().to(prop.getProperty("GAdventures_nz"));
		}

		if (Supplier.equalsIgnoreCase("Dragoman")) {
			Driver.navigate().to(prop.getProperty("Dragoman_nz"));
		}

		if (Supplier.equalsIgnoreCase("1Source")) {
			Driver.navigate().to(prop.getProperty("1Source_nz"));
		}
		if (Supplier.equalsIgnoreCase("TrekAmerica")) {
			Driver.navigate().to(prop.getProperty("TrekAmerica_nz"));
		}
		if (Supplier.equalsIgnoreCase("GrandAmerica")) {
			Driver.navigate().to(prop.getProperty("GrandAmerica_nz"));
		}
		if (Supplier.equalsIgnoreCase("Radical")) {
			Driver.navigate().to(prop.getProperty("Radical_nz"));
		}

	}

	public void LaunchUSURL(String Supplier) throws Exception {
		InputStream input = new FileInputStream("UtilitiesProd.properties");
		Properties prop = new Properties();
		prop.load(input);
		if (Supplier.equalsIgnoreCase("Contiki")) {
			Driver.navigate().to(prop.getProperty("Contiki_us"));
		}

		if (Supplier.equalsIgnoreCase("GAdventures")) {
			Driver.navigate().to(prop.getProperty("GAdventures_us"));
		}

		if (Supplier.equalsIgnoreCase("Dragoman")) {
			Driver.navigate().to(prop.getProperty("Dragoman_us"));
		}

		if (Supplier.equalsIgnoreCase("1Source")) {
			Driver.navigate().to(prop.getProperty("1Source_us"));
		}
		if (Supplier.equalsIgnoreCase("TrekAmerica")) {
			Driver.navigate().to(prop.getProperty("TrekAmerica_us"));
		}
		if (Supplier.equalsIgnoreCase("GrandAmerica")) {
			Driver.navigate().to(prop.getProperty("GrandAmerica_us"));
		}
		if (Supplier.equalsIgnoreCase("Radical")) {
			Driver.navigate().to(prop.getProperty("Radical_us"));
		}

	}

	public void AcceptCookies() {
		try {
			Driver.findElement(By.id("sta-cookie-save-all-button")).click();
		} catch (Exception e) {
			System.out.println("Cookies already accepted");
		}
	}

}

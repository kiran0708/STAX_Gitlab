package CodeImplementation;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import Base.BaseUtil;
import ObjectRepository.TourObjects;

public class Tour extends BaseUtil implements TourObjects {
	Home Home = new Home(Driver);
	public Tour(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//div[@class='photo-container']")
	public List<WebElement> Tours;

	@FindBy(how = How.XPATH, using = "//span[contains(@class,'trip-price-inner ng-binding')]")
	public List<WebElement> priceXpath;

	@FindBy(how = How.XPATH, using = "//span[@class='b-price ']")
	public WebElement Price_TripDetailsPage;

	@Override
	public String SelectRandomTour() {
		Random r = new Random();
		int randomValue = r.nextInt(Tours.size());
		String TourPrice = priceXpath.get(randomValue).getText();
		System.out.println("The Tour Price is : " + TourPrice);
		Tours.get(randomValue).click();
		return TourPrice;
	}

	@Override
	public String GetPriceFromTripDetailsPage() {
		String Price=Price_TripDetailsPage.getText();
		return Price;
	}

	@Override
	public void SelectLowestPriceTour() throws Exception {
		Home.SelectSortOption("PriceLowest");
		Thread.sleep(5000);
		Tours.get(0).click();

	}

	@Override
	public void SelectHighestPriceTour() throws Exception {
		Home.SelectSortOption("PriceHigest");
		Thread.sleep(5000);
		Tours.get(0).click();

	}



}

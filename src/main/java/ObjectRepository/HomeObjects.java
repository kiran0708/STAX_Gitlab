package ObjectRepository;

import org.openqa.selenium.By;

public interface HomeObjects {
	
	public String SelectSortOption(String SortOption) throws InterruptedException;
	
	public void VerifySortResults(String SortOption);
	
	public void VerifySortResultsPerDay(String SortOption);
		
	public void ClickReset();
	
	public void userSearchesonAutoSearch(String SearchString);
	
	public void userSearchesCountryFromAutoSearch(String Country);
	
	public void UserScrollTillTheResetButtonIsFound() throws InterruptedException;
	
	public void VerifyCountOfTours();
	
	public void MovePriceSliderToLeft() throws Exception;
	
	public void MovePriceSliderToRight() throws Exception;
	
	public void EnterLowestPriceRance() throws Exception;
	
	public void EnterHighestPriceRance() throws Exception;
	
	public void VerifyLowestPriceAccordingToPriceFilters() throws Exception;
	
	public void VerifyHighestPriceAccordingToPriceFilters() throws Exception;

    public void MoveDurationSliderToLeft() throws Exception;
	
	public void MoveDurationSliderToRight() throws Exception;
	
	public void EnterLowestDuration() throws Exception;
	
	public void EnterHighestDuration() throws Exception;
	
	public void VerifyLowestDurationAccordingToDurationFilters() throws Exception;
	
	public void VerifyHighestPriceAccordingToDurationFilters() throws Exception;
	
	public void ClickOnListView() throws Exception;
	
	public void ClickOnGridView() throws Exception;
	
	public void VerifyToursDisplayInListView() throws Exception;

	public void VerifyToursDisplayInGridView() throws Exception;
	
	public void SelectFourthMonthFromNow();
	
	
	
}

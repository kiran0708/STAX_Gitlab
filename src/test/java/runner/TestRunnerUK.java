package runner;


import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.github.mkolisnyk.cucumber.reporting.CucumberResultsOverview;

import Base.funtions;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = { "src/test/java/features" },tags="@UK", plugin = {
		"com.vimalselvam.cucumber.listener.ExtentCucumberFormatter:","rerun:target/rerun.txt", "json:target/cucumber-reports/cucumber.json" }, glue = "steps")
public class TestRunnerUK extends AbstractTestNGCucumberTests {
	
	@BeforeSuite
	public static void setup() {

		funtions commonfunctions = new funtions();
		commonfunctions.reportfunction();
		

	}

	@AfterSuite(alwaysRun=true)
	public void setupp(){
		
	CucumberResultsOverview results = new CucumberResultsOverview();
	results.setOutputDirectory("target");
	results.setOutputName("cucumber-results");
	results.setSourceFile("target/cucumber-reports/cucumber.json");
	try {
		results.execute();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	}

}

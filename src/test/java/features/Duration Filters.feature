Feature: Verify if Duration Filters are working properly

# UK Division 

@UK
Scenario Outline: verify Duration filters through Slider for <Supplier>
Given User Launches UK <Supplier> Application
And User Slides the Duration for Minimum range
Then Lowest Duration should be adjusted
And User Slides the Duration for Maximum range
Then Highest Duration should be adjusted

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|


@UK
Scenario Outline: verify Duration filters through Text box for <Supplier> 
Given User Launches UK <Supplier> Application
And User Enters the Duration for Minimum range
Then Lowest Duration should be adjusted
And User Enters the Duration for Maximum range
Then Highest Duration should be adjusted

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|

# US Division 

@US
Scenario Outline: verify Duration filters through Slider for <Supplier>
Given User Launches US <Supplier> Application
And User Slides the Duration for Minimum range
Then Lowest Duration should be adjusted
And User Slides the Duration for Maximum range
Then Highest Duration should be adjusted

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|


@US
Scenario Outline: verify Duration filters through Text box for <Supplier> 
Given User Launches US <Supplier> Application
And User Enters the Duration for Minimum range
Then Lowest Duration should be adjusted
And User Enters the Duration for Maximum range
Then Highest Duration should be adjusted

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|

# AU Division 

@AU
Scenario Outline: verify Duration filters through Slider for <Supplier>
Given User Launches AU <Supplier> Application
And User Slides the Duration for Minimum range
Then Lowest Duration should be adjusted
And User Slides the Duration for Maximum range
Then Highest Duration should be adjusted

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|


@AU
Scenario Outline: verify Duration filters through Text box for <Supplier> 
Given User Launches AU <Supplier> Application
And User Enters the Duration for Minimum range
Then Lowest Duration should be adjusted
And User Enters the Duration for Maximum range
Then Highest Duration should be adjusted

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|

# NZ Division 

@NZ
Scenario Outline: verify Duration filters through Slider for <Supplier>
Given User Launches NZ <Supplier> Application
And User Slides the Duration for Minimum range
Then Lowest Duration should be adjusted
And User Slides the Duration for Maximum range
Then Highest Duration should be adjusted

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|


@NZ
Scenario Outline: verify Duration filters through Text box for <Supplier> 
Given User Launches NZ <Supplier> Application
And User Enters the Duration for Minimum range
Then Lowest Duration should be adjusted
And User Enters the Duration for Maximum range
Then Highest Duration should be adjusted

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|
Feature: Verify the Autosearch complete function

@UK
Scenario Outline: User searches for <Country> with <SearchString> for UK <Supplier>
Given User Launches AU <Supplier> Application
Given User provides <SearchString>
And Selects the <Country> from the list


Examples:
|Supplier|SearchString|Country|
|Contiki|uni|United Kingdom|
|Contiki|Eur|Europe|


@US
Scenario Outline: User searches for <Country> with <SearchString> for US <Supplier>
Given User Launches US <Supplier> Application
Given User provides <SearchString>
And Selects the <Country> from the list


Examples:
|Supplier|SearchString|Country|
|Contiki|uni|United Kingdom|
|Contiki|Eur|Europe|
|GAdventures|uni|United Kingdom|
|GAdventures| Eur|Europe|
|1Source|uni|United Kingdom|
|1Source|Eur|Europe|
|Dragoman|uni|United Kingdom|
|Dragoman|Eur|Europe|
|GrandAmerica|uni|United Kingdom|
|GrandAmerica|Eur|Europe|
|TrekAmerica|uni|United Kingdom|
|TrekAmerica|Eur|Europe|
|Radical|uni|United Kingdom|
|Radical|Eur|Europe|


@AU
Scenario Outline: User searches for <Country> with <SearchString> for AU <Supplier>
Given User Launches AU <Supplier> Application
Given User provides <SearchString>
And Selects the <Country> from the list


Examples:
|Supplier|SearchString|Country|
|Contiki|uni|United Kingdom|
|Contiki|Eur|Europe|
|GAdventures|uni|United Kingdom|
|GAdventures| Eur|Europe|
|1Source|uni|United Kingdom|
|1Source|Eur|Europe|
|Dragoman|uni|United Kingdom|
|Dragoman|Eur|Europe|
|GrandAmerica|uni|United Kingdom|
|GrandAmerica|Eur|Europe|
|TrekAmerica|uni|United Kingdom|
|TrekAmerica|Eur|Europe|
|Radical|uni|United Kingdom|
|Radical|Eur|Europe|

@NZ
Scenario Outline: User searches for <Country> with <SearchString> for NZ <Supplier>
Given User Launches NZ <Supplier> Application
Given User provides <SearchString>
And Selects the <Country> from the list


Examples:
|Supplier|SearchString|Country|
|Contiki|uni|United Kingdom|
|Contiki|Eur|Europe|
|GAdventures|uni|United Kingdom|
|GAdventures| Eur|Europe|
|1Source|uni|United Kingdom|
|1Source|Eur|Europe|
|Dragoman|uni|United Kingdom|
|Dragoman|Eur|Europe|
|GrandAmerica|uni|United Kingdom|
|GrandAmerica|Eur|Europe|
|TrekAmerica|uni|United Kingdom|
|TrekAmerica|Eur|Europe|
|Radical|uni|United Kingdom|
|Radical|Eur|Europe|
Feature: Verify If Price Filters are working properly

# UK Division 

@Upppp
Scenario Outline: verify price filters through Slider for <Supplier>
Given User Launches UK <Supplier> Application
And User Slides the price for Minimum range
Then Lowest price tour should be adjusted
And User Slides the price for Maximum range
Then Highest price tour should be adjusted

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|

@UK
Scenario Outline: verify price filters through Text box for <Supplier> 
Given User Launches UK <Supplier> Application
And User Enters the price for Minimum range
Then Lowest price tour should be adjusted
And User Enters the price for Maximum range
Then Highest price tour should be adjusted

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|

# US Division 

@US
Scenario Outline: verify price filters through Slider for <Supplier>
Given User Launches US <Supplier> Application
And User Slides the price for Minimum range
Then Lowest price tour should be adjusted
And User Slides the price for Maximum range
Then Highest price tour should be adjusted

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|

@US
Scenario Outline: verify price filters through Text box for <Supplier> 
Given User Launches US <Supplier> Application
And User Enters the price for Minimum range
Then Lowest price tour should be adjusted
And User Enters the price for Maximum range
Then Highest price tour should be adjusted

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|

# AU Division 

@AU
Scenario Outline: verify price filters through Slider for <Supplier>
Given User Launches AU <Supplier> Application
And User Slides the price for Minimum range
Then Lowest price tour should be adjusted
And User Slides the price for Maximum range
Then Highest price tour should be adjusted

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|

@AU
Scenario Outline: verify price filters through Text box for <Supplier> 
Given User Launches AU <Supplier> Application
And User Enters the price for Minimum range
Then Lowest price tour should be adjusted
And User Enters the price for Maximum range
Then Highest price tour should be adjusted

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|

# NZ Division 

@NZ
Scenario Outline: verify price filters through Slider for <Supplier>
Given User Launches NZ <Supplier> Application
And User Slides the price for Minimum range
Then Lowest price tour should be adjusted
And User Slides the price for Maximum range
Then Highest price tour should be adjusted

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|

@NZ
Scenario Outline: verify price filters through Text box for <Supplier> 
Given User Launches NZ <Supplier> Application
And User Enters the price for Minimum range
Then Lowest price tour should be adjusted
And User Enters the price for Maximum range
Then Highest price tour should be adjusted

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|

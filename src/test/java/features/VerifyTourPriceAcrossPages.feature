Feature: Verify Tour Price Consistency Across Pages

@UK
Scenario Outline: Verify if the tour price is displayed correctly across all Pages for <Supplier>
Given User Launches UK <Supplier> Application
Given User Selects a random tour
Then The Tour Price should be same in Tour Details Page
And User clicks on Book Now
Then The Tour Price Should be same in Trip Summary
	

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|

@US
Scenario Outline: Verify if the tour price is displayed correctly across all Pages for <Supplier>
Given User Launches US <Supplier> Application
Given User Selects a random tour
Then The Tour Price should be same in Tour Details Page
And User clicks on Book Now
Then The Tour Price Should be same in Trip Summary
	

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|

@AU
Scenario Outline: Verify if the tour price is displayed correctly across all Pages for <Supplier>
Given User Launches AU <Supplier> Application
Given User Selects a random tour
Then The Tour Price should be same in Tour Details Page
And User clicks on Book Now
Then The Tour Price Should be same in Trip Summary
	

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|

@NZ
Scenario Outline: Verify if the tour price is displayed correctly across all Pages for <Supplier>
Given User Launches NZ <Supplier> Application
Given User Selects a random tour
Then The Tour Price should be same in Tour Details Page
And User clicks on Book Now
Then The Tour Price Should be same in Trip Summary
	

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|
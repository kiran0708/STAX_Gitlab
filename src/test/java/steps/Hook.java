package steps;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.Capabilities;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.model.Test;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

//import  com.vimalselvam.cucumber.listener.Reporter;
import com.vimalselvam.cucumber.listener.Reporter;

import Base.BaseUtil;
import Base.funtions;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import gherkin.formatter.model.Result;
import io.github.bonigarcia.wdm.WebDriverManager;

import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.UnexpectedAlertBehaviour;

public class Hook extends BaseUtil {

	public BaseUtil base;
	
	
	
	
	@Before
	public void InitializeTest(Scenario scenario) throws MalformedURLException {
		
		Capabilities chromeCapabilities = DesiredCapabilities.chrome();
		
		
		/*
		 * System.out.println( WebDriverManager.firefoxdriver().getVersions());
		 * WebDriverManager.firefoxdriver().setup(); Driver = new FirefoxDriver();
		 */
		 
		//  System.setProperty("webdriver.chrome.driver","TestConfig/chromedriver.exe");
		/*WebDriverManager.chromedriver().setup();*/
	/*	  ChromeOptions options = new ChromeOptions();
		  options.setExperimentalOption("useAutomationExtension", false);
		  options.addArguments("start-maximized");
		  options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
                 UnexpectedAlertBehaviour.IGNORE);*/
		  //options.addArguments("----headless");
		  	RemoteWebDriver Driver = new RemoteWebDriver(new URL("http://127.0.0.1:8888"), chromeCapabilities);
       //  Driver = new ChromeDriver(options);
		  Driver.manage().window().maximize();
		 Driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	
		
		
		
		

	}

	@After
	public void TearDownTest(Scenario scenario) throws IOException, InterruptedException {
		
		
		if (scenario.getStatus().equals(Result.FAILED)) {

			funtions commonfunctions = new funtions();
			String screenShotPath = commonfunctions.screenshot(Driver, System.currentTimeMillis());
			Reporter.addScreenCaptureFromPath(screenShotPath);
			System.out.println(scenario.getName());
			Driver.quit();

		}
		Driver.quit();

	}


	public static void captureScreenshot(WebDriver driver, String screenshotName) {

		try {
			TakesScreenshot ts = (TakesScreenshot) driver;

			File source = ts.getScreenshotAs(OutputType.FILE);

			FileUtils.copyFile(source, new File("./screenshot/" + screenshotName + ".png"));

			System.out.println("Screenshot taken");

		} catch (Exception e) {

			System.out.println("Exception while taking screenshot " + e.getMessage());
		}


	}
}

package steps;

import Base.BaseUtil;
import CodeImplementation.Basic;
import CodeImplementation.Booking;
import CodeImplementation.Tour;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;


public class BookingSteps extends BaseUtil {

	Tour tour = new Tour(Driver);
	Booking book = new Booking(Driver);
	Basic basic = new Basic();


	@And("^User clicks on Book Now$")
    public void user_clicks_on_book_now() throws Throwable {
		
		
       book.ClickBookNow();
       Thread.sleep(5000);
    }

	@And("^Adjust Departure Date for Hold Booking$")
    public void adjust_departure_date_for_hold_booking() throws Throwable {
		
		
        book.AdjustDepartureDateForHold();
    }

	@Then("^Hold Booking should be created$")
    public void hold_booking_should_be_created() throws Throwable {
		
        book.VerifyHoldBooking();
    }

    @And("^Selects the Need more time RB$")
    public void selects_the_need_more_time_rb() throws Throwable {
    	
        book.ClickNeedMoreTimeRB();
    }

    @And("^Click On Continue$")
    public void click_on_continue() throws Throwable {
    	
        book.ClickContinue();
    }

    @And("^Fills the passenger details$")
    public void fills_the_passenger_details() throws Throwable {
    	
		
        book.EnterPassengerDetails();
    }

    @And("^Fills the Card Details and Billing Details$")
    public void fills_the_card_details_and_billing_details() throws Throwable {
    	
		

    }

	@And("^Accepts the Terms and Conditions$")
	public void accepts_the_terms_and_conditions() throws Throwable {
		
		
		book.AcceptBookingTerms();
	}

    @And("^Clicks On Hold Request$")
    public void clicks_on_hold_request() throws Throwable {
    	
		
        book.ClickHoldRequest();
    }

    @And("^Adjust Departure date for Deposit Booking$")
    public void adjust_departure_date_for_deposit_booking() throws Throwable {
    	
		
        book.AdjustDepartureDateForDeposit();
    }

    @And("^Adjust Departure date for Full Payment Booking$")
    public void adjust_departure_date_for_full_payment_booking() throws Throwable {
    	
		
        book.AdjustDepartureDateForFullPayment();
    }

    @And("^Selects the Pay Full Amount Option$")
    public void selects_the_pay_full_amount_option() throws Throwable {
    	
		
       book.selectPayFullAmount();
    }

    @And("^Fills the Card Details In Staging$")
    public void fills_the_card_details_in_staging() throws Throwable {
    	
		
        book.EnterValidCardDetailsStaging();
    }

    @And("^Fills the Billing Details$")
    public void fills_the_billing_details() throws Throwable {
    	
		
        book.EnterBillingDetails();
    }

    @And("^Selects the Pay Deposit Option$")
    public void selects_the_pay_deposit_option() throws Throwable {
    	
		
        book.selectPayDepositAmount();
    }

    @And("^Complete The Booking$")
	public void complete_the_booking() throws Throwable {
    	
		
		Thread.sleep(3000);
		book.ClickBookAtCheckOut();
		Thread.sleep(10000);
	}

    @Then("^FullPayment Booking Should be Done$")
    public void fullpayment_booking_should_be_done() throws Throwable {
    	
		
        book.VerifyFullPaymentBooking();
    }

    @Then("^Deposit Booking Should be Done$")
    public void deposit_booking_should_be_done() throws Throwable {
    	
		
        book.VerifyDepositBooking();
    }

    @And("^Fills the Invalid Card Details$")
    public void fills_the_invalid_card_details() throws Throwable {
    	
		
       book.EnterInvalidCardDetails();
    }


    @Then("^Payment Error Popup should be displayed$")
    public void payment_error_popup_should_be_displayed() throws Throwable {
        Thread.sleep(5000);
        book.VerifyInvalidPayment();
    }
}

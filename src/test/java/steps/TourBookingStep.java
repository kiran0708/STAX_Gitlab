package steps;

import  com.vimalselvam.cucumber.listener.Reporter;

import Base.BaseUtil;
import Base.funtions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import pages.HomePage;
import pages.ToursPage;

public class TourBookingStep extends BaseUtil {

	/*
	 * funtions commonfunctions = new funtions(); ToursPage ToursPage = new
	 * ToursPage(Driver); HomePage HomePage = new HomePage(Driver);
	 * 
	 * @Given("^user navigates to (.+)$") public void user_navigates_to(String URL)
	 * throws Throwable { Driver.navigate().to(URL); int cookies =
	 * HomePage.AcceptCookies.size(); if (cookies > 0) {
	 * HomePage.Click_AcceptCookies(); }
	 * 
	 * }
	 * 
	 * @And("^user sort tour by price high to low$") public void
	 * user_sort_tour_by_price_high_to_low() throws Throwable {
	 * HomePage.SortbyHighprice("PriceHigest"); String screenShotPath =
	 * commonfunctions.screenshot(Driver, System.currentTimeMillis());
	 * Reporter.addScreenCaptureFromPath(screenShotPath);
	 * 
	 * 
	 * }
	 * 
	 * @Then("^user finds a tour which has option to Hold$") public void
	 * user_finds_a_tour_which_has_option_to_Hold() throws Throwable { String
	 * tourwithdeposit = commonfunctions.getTourLinkwhichHasDeposit(Driver);
	 * Reporter.addStepLog(tourwithdeposit); String screenShotPath =
	 * commonfunctions.screenshot(Driver, System.currentTimeMillis());
	 * Reporter.addScreenCaptureFromPath(screenShotPath);
	 * Driver.navigate().to(tourwithdeposit); }
	 * 
	 * @And("^user naviagtes to tour$") public void userNaviagtesToTour() throws
	 * Throwable { String tourwithdeposit =
	 * commonfunctions.getTourLinkwhichHasDeposit(Driver);
	 * Reporter.addStepLog(tourwithdeposit); String screenShotPath =
	 * commonfunctions.screenshot(Driver, System.currentTimeMillis());
	 * Reporter.addScreenCaptureFromPath(screenShotPath); ToursPage.Booknow.click();
	 * Reporter.addStepLog("User clicks Booknow"); }
	 * 
	 * @And("^user selects tour departure date$") public void
	 * userslectsdepaturedate() throws Throwable {
	 * Reporter.addStepLog(ToursPage.TripSummary.getText());
	 * 
	 * ToursPage.Downarrow.click();
	 * Reporter.addStepLog("User clicks on down arrow at calender"); if
	 * (ToursPage.TourdepartureYear.size()>0) {
	 * ToursPage.TourdepartureYear.get(1).click(); } else
	 * {ToursPage.TourdepartureYear.get(0).click();}
	 * Reporter.addStepLog("User Selects Departure Date"); String screenShotPath =
	 * commonfunctions.screenshot(Driver, System.currentTimeMillis());
	 * Reporter.addScreenCaptureFromPath(screenShotPath); }
	 * 
	 * @And("^user click need more time to decide radio button$") public void
	 * userclicksneedmoretimeRadio() throws Throwable {
	 * 
	 * ToursPage.NeedmoretimetodecideRadiobutton.click(); ;
	 * Reporter.addStepLog("User click NeedmoretimetodecideRadiobutton");
	 * ToursPage.ContinueButton.click(); ;
	 * Reporter.addStepLog("User click ContinueButton"); String screenShotPath =
	 * commonfunctions.screenshot(Driver, System.currentTimeMillis());
	 * Reporter.addScreenCaptureFromPath(screenShotPath);
	 * 
	 * }
	 * 
	 * @Then("^user Enter user details on Booking Checkout Page$") public void
	 * userDetails() throws Throwable { commonfunctions.Dropdown(ToursPage.Gender,
	 * "Male"); commonfunctions.Dropdown(ToursPage.DOBday, "14");
	 * commonfunctions.Dropdown(ToursPage.DOBMonth, "4");
	 * commonfunctions.Dropdown(ToursPage.DOByear, "1974");
	 * commonfunctions.Dropdown(ToursPage.Nationality, "British");
	 * ToursPage.FirstName.sendKeys(commonfunctions.person.getFirstName());
	 * ToursPage.LasttName.sendKeys(commonfunctions.person.getLastName());
	 * ToursPage.FirstName.sendKeys(commonfunctions.person.getMiddleName());
	 * ToursPage.Emailaddress.sendKeys("statraveltestautomation@gmail.com ");
	 * ToursPage.Phonenumber.sendKeys("4444444444");
	 * Reporter.addStepLog("User Eneters FirstName" +
	 * commonfunctions.person.getFirstName());
	 * Reporter.addStepLog("User Eneters FirstName" +
	 * commonfunctions.person.getMiddleName());
	 * Reporter.addStepLog("User Eneters FirstName" +
	 * commonfunctions.person.getLastName()); String screenShotPath =
	 * commonfunctions.screenshot(Driver, System.currentTimeMillis());
	 * Reporter.addScreenCaptureFromPath(screenShotPath); }
	 * 
	 * @And("^user accepts supplier and STA terms$") public void
	 * userclickstaandSupplierterms() throws Throwable {
	 * 
	 * ToursPage.SuppilerTerms.click(); ToursPage.STAterms.click(); String
	 * screenShotPath = commonfunctions.screenshot(Driver,
	 * System.currentTimeMillis());
	 * Reporter.addScreenCaptureFromPath(screenShotPath);
	 * 
	 * }
	 * 
	 * @And("^user click HoldRequest$") public void userclickHoldRequest() throws
	 * Throwable {
	 * 
	 * ToursPage.HoldRequestButton.click();
	 * 
	 * }
	 * 
	 * @And("^user clicks on confirm now button$") public void userclickConfirmNow()
	 * throws Throwable {
	 * 
	 * ToursPage.ConfirmtourNow.click();
	 * 
	 * }
	 */

}

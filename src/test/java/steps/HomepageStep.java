
package steps;

import com.vimalselvam.cucumber.listener.Reporter;

import Base.BaseUtil;
import Base.funtions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import CodeImplementation.Home;

public class HomepageStep extends BaseUtil {

	funtions commonfunctions = new funtions();
	//HomePage HomePage = new HomePage(Driver);
	Home Home = new Home(Driver);

	@Given("^User provides (.+)$")
	public void user_provides(String searchstring) throws Throwable {
		Home.userSearchesonAutoSearch(searchstring);
	}

	@And("^Selects the (.+) from the list$")
	public void selects_the_from_the_list(String country) throws Throwable {
		Home.userSearchesCountryFromAutoSearch(country);
	}

	@Given("^User selects the (.+)$")
	public void user_selects_the(String sortoption) throws Throwable {
		System.out.println("Sort Order Test For : "+sortoption);
		Thread.sleep(2000);
		Home.SelectSortOption(sortoption);
		Thread.sleep(5000);
	}

	@Then("^Verify the Results for (.+)$")
	public void verify_the_results_for(String sortoption) throws Throwable {
		Thread.sleep(3000);
		if (sortoption.equalsIgnoreCase("PriceLowest") || sortoption.equalsIgnoreCase("PriceHigest")) {
			Home.VerifySortResults(sortoption);
		}else {
			Home.VerifySortResultsPerDay(sortoption);
		}
	}

	@Then("^Lowest price tour should be adjusted$")
	public void lowest_price_tour_should_be_adjusted() throws Throwable {
		Home.VerifyLowestPriceAccordingToPriceFilters();
	}

	@Then("^Highest price tour should be adjusted$")
	public void highest_price_tour_should_be_adjusted() throws Throwable {
		Home.VerifyHighestPriceAccordingToPriceFilters();
	}

	@And("^User Slides the price for Minimum range$")
	public void user_slides_the_price_for_minimum_range() throws Throwable {
		Home.MovePriceSliderToRight();
		Thread.sleep(2500);
	}

	@And("^User Slides the price for Maximum range$")
	public void user_slides_the_price_for_maximum_range() throws Throwable {
		Home.MovePriceSliderToLeft();
		Thread.sleep(2500);
	}

	@And("^User Enters the price for Minimum range$")
	public void user_enters_the_price_for_minimum_range() throws Throwable {
		Home.EnterLowestPriceRance();
	}

	@And("^User Enters the price for Maximum range$")
	public void user_enters_the_price_for_maximum_range() throws Throwable {
		Home.EnterHighestPriceRance();
	}

	@Then("^Lowest Duration should be adjusted$")
	public void lowest_duration_should_be_adjusted() throws Throwable {
		Home.VerifyLowestDurationAccordingToDurationFilters();
	}

	@Then("^Highest Duration should be adjusted$")
	public void highest_duration_should_be_adjusted() throws Throwable {
		Home.VerifyHighestPriceAccordingToDurationFilters();
	}

	@And("^User Slides the Duration for Minimum range$")
	public void user_slides_the_duration_for_minimum_range() throws Throwable {
		Home.MoveDurationSliderToRight();
	}

	@And("^User Slides the Duration for Maximum range$")
	public void user_slides_the_duration_for_maximum_range() throws Throwable {
		Home.MoveDurationSliderToLeft();
	}

	@And("^User Enters the Duration for Minimum range$")
	public void user_enters_the_duration_for_minimum_range() throws Throwable {
		Home.EnterLowestDuration();
	}

	@And("^User Enters the Duration for Maximum range$")
	public void user_enters_the_duration_for_maximum_range() throws Throwable {
		Home.EnterHighestDuration();
	}

	@And("^Selects the departure month as three months from now in filters$")
    public void selects_the_departure_month_as_three_months_from_now_in_filters() throws Throwable {
        Home.SelectFourthMonthFromNow();
        Thread.sleep(5000);
    }
	
	@Given("^User naviagtes to tours page (.+)$")
	public void user_naviagtes_to_tours_page_PriceLowest(String URL) throws Throwable {
		/*
		 * Driver.navigate().to(URL); int cookies = HomePage.AcceptCookies.size(); if
		 * (cookies > 0) { HomePage.Click_AcceptCookies(); }
		 */

	}

	@Then("^Tours should be displayed in List view$")
	public void tours_should_be_displayed_in_list_view() throws Throwable {
		Home.VerifyToursDisplayInListView();
	}

	@Then("^Tours should be displayed in Grid view$")
	public void tours_should_be_displayed_in_grid_view() throws Throwable {
		Home.VerifyToursDisplayInGridView();
	}

	@And("^User clicks on List View$")
	public void user_clicks_on_list_view() throws Throwable {
		Home.ClickOnListView();
		Thread.sleep(5000);
	}

	@And("^User clicks on Grid view$")
	public void user_clicks_on_grid_view() throws Throwable {
		Home.ClickOnGridView();
		Thread.sleep(5000);
	}

	@And("^User select priceby from (.+)$")
	public void User_select_priceby_from(String SortByDropDown) throws Throwable {
		/*
		 * HomePage.Sortby(SortByDropDown); String screenShotPath =
		 * commonfunctions.screenshot(Driver, System.currentTimeMillis());
		 * Reporter.addScreenCaptureFromPath(screenShotPath);
		 */
	}

}
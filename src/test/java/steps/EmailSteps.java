package steps;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import Base.BaseUtil;
import CodeImplementation.Basic;
import CodeImplementation.Email;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class EmailSteps extends BaseUtil {

	Email email = new Email(Driver);
	Basic basic = new Basic();

	@Given("^User Selects a lowest price tour$")
	@Test(description ="user_selects_a_lowest_price_tour")
    public void user_selects_a_lowest_price_tour() throws Throwable {
		 Driver.findElement(By.id("sortSelect")).sendKeys("Price (Low-High)");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Driver.findElement(By.xpath("//*[@id=\"results-flex-grid\"]/div[2]/div[1]/div/a/img")).click();
    }

	  @And("^Clicks on Email us$")
	  @Test(description ="clicks_on_email_us")
	    public void clicks_on_email_us() throws Throwable {

		  	basic.SwitchToNewWindow();
	    	Thread.sleep(5000);
	    	email.ClickEmailUs();
	    }

	    @And("^Provides all the details$")
	    @Test(description ="provides_all_the_details")
	    public void provides_all_the_details() throws Throwable {
	        email.EnterEmailDetails();
	    }

	    @And("^Click on Send My Email$")
	    @Test(description ="click_on_send_my_email")
	    public void click_on_send_my_email() throws Throwable {
	       email.ClickSendMyEmail();
	    }

	    @Then("^An email will be sent$")
	    @Test(description ="an_email_will_be_sent")
	    public void an_email_will_be_sent() throws Throwable {
	        email.VerifyEmailSentText();
	    }


}

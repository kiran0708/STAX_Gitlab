package apitest;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

//import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.ITest;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

//import io.restassured.RestAssured;
//import io.restassured.specification.RequestSpecification;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class staxAPITest implements ITest {

	@Override
	public String getTestName() {
		// TODO Auto-generated method stub
		return null;
	}
	/*
	 * public ExtentReports extent; public ExtentTest test; public
	 * ExtentHtmlReporter htmlReporter; public ThreadLocal<String> testName = new
	 * ThreadLocal<>();
	 * 
	 * 
	 * @BeforeTest public void extentReportSetup() {
	 * 
	 * htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") +
	 * "/Reports/STAX-APITest.html"); extent = new ExtentReports();
	 * extent.attachReporter(htmlReporter);
	 * 
	 * htmlReporter.config().setDocumentTitle("SATX Automation Report");
	 * htmlReporter.config().setReportName("SATX -API test ");
	 * htmlReporter.config().setTheme(Theme.DARK);
	 * 
	 * //htmlReporter.config().enableTimeline(true);
	 * 
	 * 
	 * 
	 * extent.setSystemInfo("STAX-API Testing for AU Division",
	 * "Stage Environment");
	 * 
	 * }
	 * 
	 * @AfterTest public void endReport() { extent.flush(); }
	 * 
	 * 
	 * 
	 * @org.testng.annotations.BeforeMethod public void BeforeMethod(Method method,
	 * Object[] testData){ testName.set(method.getName() + "_" + testData[0]); }
	 * 
	 * 
	 * 
	 * @Override public String getTestName() { return testName.get(); }
	 * 
	 * 
	 * This method is helps to parameterise test using DataProvider
	 *
	 * 
	 * 
	 * @DataProvider(parallel=true) public Object[][] ProvideData() throws Exception
	 * {
	 * 
	 * String envFilePath = System.getProperty("user.dir") +
	 * "//testdata//urldata.xls"; Workbook workbook = Workbook.getWorkbook(new
	 * File(envFilePath)); // add sheename here Sheet sheet =
	 * workbook.getSheet("AU"); // return number of rows(records) int records =
	 * sheet.getRows() - 1; int currentPosition = 1; Object[][] values = new
	 * Object[records][3]; for (int i = 0; i < records; i++, currentPosition++) { //
	 * loop over the rows for (int j = 0; j < 3; j++) values[i][j] =
	 * sheet.getCell(j, currentPosition).getContents(); } workbook.close(); return
	 * values; }
	 * 
	 * 
	 * This method is helps to test http methods defined in test data file
	 *
	 * 
	 * 
	 * @Test(dataProvider = "ProvideData",groups={"staxAPItest"},
	 * threadPoolSize=20,invocationCount=1) public void verify_post_method(String
	 * URL, String reguestMethod, String StatusCode) throws RowsExceededException,
	 * BiffException, WriteException, IOException {
	 * 
	 * 
	 * 
	 * String baseurl = "https://austaging.staexperiences.com/trip/"; test =
	 * extent.createTest("verify-hhtp-post for"+"          "+baseurl+URL);
	 * 
	 * RestAssured.baseURI = baseurl; RequestSpecification request =
	 * RestAssured.given(); JSONObject requestParams = new JSONObject();
	 * request.header("Content-Type", "application/json");
	 * request.body(requestParams.toJSONString()); io.restassured.response.Response
	 * response = request.post(URL); int statusCode = response.getStatusCode();
	 * String ouputstatus = Integer.toString(statusCode); String body =
	 * response.getBody().asString();
	 * 
	 * 
	 * test.log(Status.INFO, MarkupHelper.createLabel(baseurl+URL,
	 * ExtentColor.LIME)); test.log(Status.INFO,
	 * MarkupHelper.createLabel("response status  "+ouputstatus, ExtentColor.RED));
	 * // below line would fetch the status code and validate contains in response
	 * body
	 * 
	 * switch (statusCode) { case 200: logger("Status Code for this URL is" +
	 * ouputstatus + "     " + baseurl + URL);
	 * 
	 * // if tour is inactive if (body.contains("row div404Error")) {
	 * //logger("Found we are sorry ");
	 * Assert.assertTrue(body.contains("We're sorry"),
	 * "The trip you've selected cannot be found or is no longer available. Check out the below for more ideas or email us if you need help.\r\n"
	 * + " is not displayed ");
	 * Assert.assertTrue(body.contains("You might also like"),
	 * "You might also like is not displayed"); } // if tour is active else if
	 * (body.contains("btn-sta btn-primary btn-book")) {
	 * logger("Tour is active and validating Book Now Button is enabled");
	 * logger("active book button ");
	 * Assert.assertTrue(body.contains("btn-sta btn-primary btn-book"));
	 * 
	 * } // if tour is inactive else if
	 * (body.contains("btn-sta btn-primary btn-book txt404")) {
	 * logger("found we are sorry and now validating Book Now button is disabled ");
	 * Assert.assertTrue(body.contains("btn-sta btn-primary btn-book txt404"));
	 * Assert.assertTrue(body.contains("We're sorry"));
	 * Assert.assertTrue(body.contains("You might also like")); } break;
	 * 
	 * case 300: logger("Status Code for this URL is" + ouputstatus + "     " +
	 * baseurl + URL); if (body.contains("row div404Error")) {
	 * Assert.assertTrue(body.contains("We're sorry"));
	 * Assert.assertTrue(body.contains("You might also like")); } // if tour is
	 * active else if (body.contains("btn-sta btn-primary btn-book")) {
	 * Assert.assertTrue(body.contains("btn-sta btn-primary btn-book")); }
	 * 
	 * // if tour is inactive else if
	 * (body.contains("btn-sta btn-primary btn-book txt404")) {
	 * Assert.assertTrue(body.contains("We're sorry"));
	 * Assert.assertTrue(body.contains("You might also like")); }
	 * 
	 * else if (body.contains("btn-primary emailPopUp")) {
	 * Assert.assertTrue(body.contains("Email Us"));
	 * Assert.assertTrue(body.contains("You might also like")); } break; case 404:
	 * logger("Status Code for this URL is" + ouputstatus + "     " + baseurl +
	 * URL); break; case 500: logger("Status Code for this URL is" + ouputstatus +
	 * "     " + baseurl + URL); break; }
	 * 
	 * 
	 * 
	 * assertTrue(ouputstatus.equalsIgnoreCase("200") ||
	 * ouputstatus.equalsIgnoreCase("301")
	 * ,"Repose for the requested url found to be        "+ouputstatus);
	 * 
	 * 
	 * 
	 * }
	 * 
	 * 
	 * This method is helps to logger the output into txt file
	 *
	 * 
	 * 
	 * public static void logger(String message) throws IOException { PrintWriter
	 * out = new PrintWriter( new FileWriter(System.getProperty("user.dir") +
	 * "//testdata//output.txt", true), true); out.println(message); out.close(); }
	 * 
	 * @AfterMethod public void getResult(ITestResult result) throws Exception {
	 * if(result.getStatus() == ITestResult.FAILURE) { //MarkupHelper is used to
	 * display the output in different colors test.log(Status.FAIL,
	 * MarkupHelper.createLabel(result.getName() + " - Test Case Failed",
	 * ExtentColor.RED)); test.log(Status.FAIL,
	 * MarkupHelper.createLabel(result.getThrowable() + " - Test Case Failed",
	 * ExtentColor.RED));
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * } else if(result.getStatus() == ITestResult.SKIP){ //logger.log(Status.SKIP,
	 * "Test Case Skipped is "+result.getName()); test.log(Status.SKIP,
	 * MarkupHelper.createLabel(result.getName() + " - Test Case Skipped",
	 * ExtentColor.ORANGE)); } else if(result.getStatus() == ITestResult.SUCCESS) {
	 * test.log(Status.PASS,
	 * MarkupHelper.createLabel(result.getName()+" Test Case PASSED",
	 * ExtentColor.GREEN)); }
	 * 
	 * }
	 */

}
Feature: User makes tour booking 

Scenario Outline: User Naviagtes to tour page and makes booking 

	Given user navigates to <url> 
	And user sort tour by price high to low 
	Then user finds a tour which has option to Hold 
	And user naviagtes to tour 
	And user selects tour departure date
	And user click need more time to decide radio button 
	Then user Enter user details on Booking Checkout Page
	And user accepts supplier and STA terms
	And user click HoldRequest
	And user clicks on confirm now button
	
	Examples: 

		|               url                                                 |   
		|http://usstaging.staexperiences.com/search/two?suppliers.items=CO  |
		
		

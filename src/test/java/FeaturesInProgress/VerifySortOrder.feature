Feature: User Verifies the Sort Order Functionality

  Scenario Outline: User Naviagtes to division <Division> and view tours
    Given User naviagtes to tours page <Division>
    And User select priceby from <SortByDropDown>
  
    Examples: 
      | Division                                                           | SortByDropDown |
      | http://usstaging.staexperiences.com/search/two?suppliers.items=CO  | PriceLowest |
    